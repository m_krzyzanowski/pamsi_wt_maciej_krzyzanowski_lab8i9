/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>
#include <exception>
#include "sekwencja.h"
#include "kolejka_prior.h"

using namespace std;

class DontExistException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podany element nie istnieje.";
	}
};

template <typename Typ> class Graph;
template <typename Typ> class Vertex;
template <typename Typ> class Edge;

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Edge
{
private:
    Typ wartosc; //wartosc krawedzi
    Vertex<Typ> *poczatek; //referencja do wierzcholka poczatkowego
    Vertex<Typ> *koniec; //referencja do wierzcholka koncowego
    Sekwencja<Edge<Typ>*> *I_poczatku; //referencja do listy sasiedztwa wierzcholka poczatkowego
    Sekwencja<Edge<Typ>*> *I_konca; //referencja do listy sasiedztwa wierzcholka koncowego
    Edge *pozycja; //referencja do pozycji na liscie krawedzi

    int label; //etukieta wykorzystywane podczas trawersacji grafu (wartosc: -1(krawedz powrotna), 0(krawedzi nieodwiedzona), 1(krawedz odwiedzona))
public:
    Edge()
    {
        poczatek = koniec = NULL;
        I_poczatku = I_konca = NULL;
        pozycja = NULL;
    }
    ~Edge()
    {
        delete poczatek, koniec;
        delete I_poczatku, I_konca;
        delete pozycja;
    }
    int getLabel() {return label;}
    void setLabel(int lab) {label=lab;}
    Typ getWartosc() {return wartosc;}
    Vertex<Typ>* getPoczatek() {return poczatek;}
    Vertex<Typ>* getKoniec() {return koniec;}

    friend class Vertex<Typ>;
    friend class Graph<Typ>;
};

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Vertex
{
private:
    Typ wartosc; //wartosc wierzcholka
    Vertex* pozycja; //referencja do polozenia na liscie krawedzi
    Sekwencja<Edge<Typ>*> I; //lista sasiedztwa

    int label; //etukieta wykorzystywane podczas trawersacji grafu (wartosci: false-wierzcholek nieodwiedzony; true-wierzcholek odwiedzony)
public:
    Vertex() {pozycja = NULL;}
    ~Vertex() {delete pozycja;}

    int getLabel() {return label;}
    void setLabel(int lab) {label=lab;}
    Typ getWartosc() {return wartosc;}

    friend class Edge<Typ>;
    friend class Graph<Typ>;
};

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Graph
{
private:
    Sekwencja<Vertex<Typ>*> V; //lista wskaznikow na wierzcholki
    Sekwencja<Edge<Typ>*> E; //lista wskaznikow na krawedzie

    Vertex<Typ>* findVert(Typ); //metoda wyszukuje wierzcholek na podstawie podanej wartosci IN: wartosc wierz. OUT: wsk. na wierzcholek
    Edge<Typ>* findEdge(Typ); //metoda wyszukuje krawedz na podstawie podanej wartosci IN: wartosc kraw. OUT: wsk na krawedz

public:

/*metody dostepu*/
    void insertVert(Typ o); //metoda dodaje nowy wierzcholek IN: o-wartosc wierzcholka
    void removeVert(Typ v); //metoda usuwa wierzcholek IN: v-wartosc wierzcholka
    void insertEdge(Typ v,Typ w,Typ o); //metoda dodaje nowy wierzcholek IN: (v,w)-krawedzie wierzcholka o-wartosc wierzcholka
    void removeEdge(Typ e); //metoda usuwa krawedz  IN: e-wartosc wierzcholka
    int endVertices(Typ); //metoda zwraca tablice dwoch koncowych wierzcholkow krawedzi e
    Vertex<Typ>* opposite(Vertex<Typ>* v, Edge<Typ>* e); //metoda wyswietla przeciwlegly wierzcholek do v wzgledem e
    bool areAdjacent(Typ v, Typ w); //jesli wierzcholki v oraz w sa sasiednie zwraca true w przeciwnym wypadku false
    void replaceVert(Typ v, Typ x); //metoda zamienia wartosci wierzcholka v oraz x
    void replaceEdge(Typ e, Typ x); //metoda zamienia wartosci krawedzi e oraz x
    Edge<Typ>* incidentEdges(Vertex<Typ>*,int); //metoda zwraca krawedzie przylegajace do v
    Vertex<Typ>* vertices(int); //metoda zwraca wskaznik na wierzcholek
    Edge<Typ>* edges(int); //metoda zwraca wskaznik na krawedz
    void clear(); //metoda usuwa wszystkie skladniki grafu
    int sizeVert(); //metoda zwraca ilosc wierzcholkow w grafie
    int sizeEdge(); //metoda zwraca ilsc krawedzi w grafie
    int sizeIncident(Vertex<Typ>* v);//metoda zwraca ilosc wierzcholkow przylegajacych do v


    friend class Edge<Typ>;
    friend class Vertex<Typ>;
};


////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::insertVert(Typ wart)
{
    Vertex<Typ> *nowy = new Vertex<Typ>;
    nowy->wartosc = wart;
    V.addFront(nowy);
    nowy->pozycja = V.front();
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::removeVert(Typ wart)
{
    int i;
    for(i=0; i<V.size(); i++)
        if(V[i]->wartosc == wart) break;

    if(i+1 > V.size()) throw DontExistException();
    else V.erase(i);
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::insertEdge(Typ wierz_od, Typ wierz_do, Typ wart)
{
    Vertex<Typ>* wierz_Poczatkowy = findVert(wierz_od);
    Vertex<Typ>* wierz_Koncowy = findVert(wierz_do);

    if (wierz_Poczatkowy == NULL || wierz_Koncowy==NULL) throw DontExistException();
    else
    {
        Edge<Typ> *nowy = new Edge<Typ>;
        //dodawanie elementow do listy krawedzi incydentnych
        wierz_Poczatkowy->I.addFront(nowy);
        wierz_Koncowy->I.addFront(nowy);
        //dodwane konca i poczatku krawedzi
        nowy->poczatek = wierz_Poczatkowy;
        nowy->koniec = wierz_Koncowy;
        //dodawani refernecji do listy sasiedztwa
        nowy->I_poczatku = &wierz_Poczatkowy->I;
        nowy->I_konca = &wierz_Koncowy->I;
        //dodanie wartosc nowej
        nowy->wartosc = wart;
        E.addFront(nowy);
        nowy->pozycja = E.front();
    }
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::removeEdge(Typ wart)
{
    int i;
    for(i=0; i<E.size(); i++)
        if(E[i]->wartosc == wart) break;

    if(i+1 > E.size()) throw DontExistException();
    else E.erase(i);
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
int Graph<Typ>::endVertices(Typ e)
{
    Edge<Typ>* edge = findEdge(e);
    if(edge == NULL) throw DontExistException();
    else
    {
        int tab[2];
        tab[0] = edge->poczatek->wartosc;
        tab[1] = edge->koniec->wartosc;
        return tab;
    }
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Vertex<Typ>* Graph<Typ>::opposite(Vertex<Typ>* vertex, Edge<Typ>* edge)
{
    if(edge->poczatek->wartosc == vertex->wartosc)
        return edge->koniec;
    else if(edge->koniec->wartosc == vertex->wartosc)
        return edge->poczatek;
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
bool Graph<Typ>::areAdjacent(Typ v, Typ w)
{
    Vertex<Typ>* vertex1 = findVert(v);
    Vertex<Typ>* vertex2 = findVert(w);

    //Warunek sprawdza czy znaleziono wierzcholki
    if (vertex1 == NULL || vertex2 == NULL) throw DontExistException();

    int wartosc2 = vertex2->wartosc;
    //Petla przeszukuje liste sasiedztwa vertex1. Jesli znajdzie wsrod nich wartosc drugiego wierzcholka zwraca TRUE
    for(int i=0; i<vertex1->I.size(); i++)
        if((vertex1->I[i]->koniec->wartosc == wartosc2) || (vertex1->I[i]->poczatek->wartosc == wartosc2))
            return true;

    return false;
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::replaceVert(Typ v, Typ x)
{
    Vertex<Typ>* vertex = findVert(v);

    if(vertex == NULL) throw DontExistException();
    else vertex->wartosc = x;
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::replaceEdge(Typ e, Typ x)
{
    Edge<Typ>* edge = findEdge(e);

    if(edge == NULL) throw DontExistException();
    else edge->wartosc = x;
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Edge<Typ>* Graph<Typ>::incidentEdges(Vertex<Typ>* v, int iter)
{
    return v->I[iter];
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Vertex<Typ>* Graph<Typ>::vertices(int iter)
{
    return V[iter];
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Edge<Typ>* Graph<Typ>::edges(int iter)
{
    return E[iter];
}

//////////////////////////////////////////////////////////////////////


template <typename Typ>
Vertex<Typ>* Graph<Typ>::findVert(Typ wart)
{
    int i;
    for(i=0; i<V.size(); i++)
        if(V[i]->wartosc == wart) break;

    if(i+1 > V.size()) return NULL;
    else return V[i];
}

template <typename Typ>
Edge<Typ>* Graph<Typ>::findEdge(Typ wart)
{
    int i;
    for(i=0; i<E.size(); i++)
        if(E[i]->wartosc == wart) break;


    if(i+1 > E.size()) return NULL;
    else return E[i];
}

template <typename Typ>
int Graph<Typ>::sizeVert()
{
    return V.size();
}

template <typename Typ>
int Graph<Typ>::sizeEdge()
{
    return E.size();
}

template <typename Typ>
int Graph<Typ>::sizeIncident(Vertex<Typ>* v)
{
    return v->I.size();
}

template <typename Typ>
void Graph<Typ>::clear()
{
    V.clear();
    E.clear();
}
